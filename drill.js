const fs = require("fs");
const http = require("http");
const url = require('url');
const uuid4 = require('uuid4');

const portNo = 3000;

const server = http.createServer((request, response) => {
    let path = url.parse(request.url).pathname;
    //let path = request.url
    if (path == "/") {
        response.writeHead(200, { "Content-type": "text/html" })
        response.write("<h1>WELCOME ALL</h1>")
        response.end();
    } else if (path == "/html") {
        fs.readFile("index.html", "utf-8", (err, data) => {
            if (err) {
                response.writeHead(400, { 'content-type': 'text/html' })
                response.write('<h1>No HTML File Found</h1>')
                response.end();
            }
            else {
                response.writeHead(200, { "Content-type": "text/html" })
                response.write(data);
                response.end();
            }
        })
    } else if (path == "/json") {
        fs.readFile("jsonData.json", "utf-8", (err, data) => {
            if (err) {
                response.writeHead(400, { 'content-type': 'text/html' })
                response.write('<h1>No JSON File Found</h1>')
                response.end();
            }
            else {
                response.writeHead(200, { "Content-type": "application/json" })
                response.write(data);
                response.end();
            }
        })
    } else if (path == "/uuid") {
        let uuidValue = uuid4();
        let uuid = { "uuid4": uuidValue };
        response.writeHead(200, { "Content-type": "application/json" });
        response.write(JSON.stringify(uuid));
    } else if(path.includes('/status/')){
        var status = path.slice(8)
        response.writeHead(200, { "Content-type": "text/plain" });
        response.write(status);
        response.end()
    } else if (path.includes("/delay/")) {
        let seconds = +path.slice(7) // return string value
        setTimeout(() => {
            response.writeHead(200, { "Content-type": "text/plain" });
            response.write("STATUS CODE : " + String(response.statusCode))
            response.end();
        }, seconds * 1000)
    } else {
        response.writeHead(400, { "Content-type": "text/html" });
        response.write("<h1>DEFAULT PATH!!</h1>")
        response.end();
    }
})
    .listen(portNo, (err) => {
        if (err) {
            console.log("Error occured while starting the server "+err);
        }
        console.log("connected to the port no " + portNo + " ..starting");
    });


    
    /* status question doubt
    //response.writeHead(parseInt(status), { "Content-type": "text/plain" });
    //response.write("STATUS CODE : " + String(response.statusCode))
    //response.write("STATUS MESSAGE : " + response.statusMessage)
    */